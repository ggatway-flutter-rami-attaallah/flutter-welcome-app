import 'dart:ui';

import 'package:flutter/material.dart';

main() {
  runApp(ass1());
}

class ass1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
            appBar: AppBar(
              leading: IconButton(
                icon: Icon(Icons.menu),
                onPressed: () {},
              ),
              title: Text(
                'Welcome',
                style: TextStyle(
                    fontFamily: 'Worksans', fontWeight: FontWeight.w700),
              ),
              actions: <Widget>[
                IconButton(
                  icon: Icon(Icons.info),
                  onPressed: () {},
                ),
                PopupMenuButton(
                    offset: Offset(-15, 39),
                    itemBuilder: (context) {
                      return [
                        PopupMenuItem(
                            child: Text(
                          'About',
                          style: TextStyle(
                              color: Colors.blue,
                              fontFamily: 'Worksans',
                              fontWeight: FontWeight.w700),
                        ))
                      ];
                    })
              ],
            ),
            body: Stack(children: [
              ImageFiltered(
                imageFilter: ImageFilter.blur(sigmaX: 4),
                child: Image(
                    height: double.infinity,
                    fit: BoxFit.cover,
                    image: AssetImage('images/jer.webp')),
              ),
              Center(
                  child: Text(
                'Rami Attaallah',
                style: TextStyle(
                    color: Colors.white,
                    fontFamily: 'Worksans',
                    fontWeight: FontWeight.w700,
                    fontSize: 30),
              )),
              Positioned(
                  left: 0,
                  right: 0,
                  bottom: 10,
                  child: Text(
                    'Elancer-GGatway',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'Worksans',
                      fontWeight: FontWeight.w400,
                    ),
                  ))
            ])));
  }
}
